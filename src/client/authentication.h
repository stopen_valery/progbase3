#ifndef AUTHENTICATION_H
#define AUTHENTICATION_H

#include <QDialog>
#include <QCryptographicHash>
#include <QMessageBox>
#include <QDebug>

#include "remotestorage.h"
#include "registration.h"

namespace Ui {
class Authentication;
}

class Authentication : public QDialog
{
    Q_OBJECT

public:
    explicit Authentication(QWidget *parent = 0);
    int get_DB_user_ID () ;
    void set_data (Storage * DB) ;
    ~Authentication();

private slots:
    void on_ok_clicked();

    void on_cancel_clicked();
    void on_Registrate_clicked();
    void closeEvent (QCloseEvent *event) ;

private:
    Ui::Authentication *ui;
    int DB_user_ID ;
    Storage *storage_ ;
};


QString HashPassword(QString const & pass) ;

#endif // AUTHENTICATION_H

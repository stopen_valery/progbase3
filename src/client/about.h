#ifndef ABOUT_H
#define ABOUT_H

#include <QDialog>
#include <QDesktopServices>

namespace Ui {
class About;
}

class About : public QDialog
{
    Q_OBJECT

public:
    explicit About(QWidget *parent = 0);
    ~About();

private slots:
    void on_aboutB_clicked();

    void on_ConB_clicked();


    void closeEvent (QCloseEvent *event) ;

    void on_donat_linkActivated(const QString &link);

private:
    Ui::About *ui;
    void hide(bool h) ;
    QMovie *movie ;
};

#endif // ABOUT_H

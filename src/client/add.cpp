#include "add.h"
#include "ui_add.h"

Add::Add(QWidget *parent, bool entity) :
    QDialog(parent),
    ui(new Ui::Add)
{
    ui->setupUi(this);
    if(entity==true)
    {
        ui->Username->setText("Place:");
        ui->Bio->setText("Description:");
        ui->Followers->setText("Comments:");
        ui->Following->setText("Likes:");
    }
    else
    {
        ui->Username->setText("Username:");
        ui->Bio->setText("Bio:");
        ui->Followers->setText("Followers:");
        ui->Following->setText("Following:");
    }
}


Add::~Add()
{
    delete ui;
}


User Add::user_data()
{

    User user;
    user.nick = ui->username->text().toStdString();
    user.followers = ui->followers->value() ;
    user.following = ui->following->value() ;
    user.bio = ui->bio->text().toStdString();
    if(image.size()!=0)
    {
        std::string im = image.toStdString() ;
        user.photo = im ;
    }

    return user;
}

Post Add::post_data()
{
    Post post;
    post.place = ui->username->text().toStdString();
    post.comments = ui->followers->value() ;
    post.likes = ui->following->value() ;
    post.description = ui->bio->text().toStdString();

    if(image.size()!=0)
    {
        std::string im = image.toStdString() ;
        post.photo = im ;
    }

    return post;
}

void Add::user_value(const User &user)
{

    ui->username->setText(QString::fromStdString(user.nick));
    ui->bio->setText(QString::fromStdString(user.bio));
    ui->followers->setValue(user.followers);
    ui->following->setValue(user.following);
    if(!user.photo.empty())
    {
        QByteArray bar = QByteArray::fromStdString(user.photo) ;
        QPixmap pixmap;
        pixmap.loadFromData(bar);
        int w = ui->photo->width();
        int h = ui->photo->height();
        ui->photo->setPixmap(pixmap.scaled(w, h, Qt::KeepAspectRatio));
    }
    else
    {
        ui->photo->clear();
    }

    image = QByteArray::fromStdString(user.photo) ;
}

void Add::post_value(const Post &post)
{

    ui->username->setText(QString::fromStdString(post.place));
    ui->bio->setText(QString::fromStdString(post.description));
    ui->followers->setValue(post.comments);
    ui->following->setValue(post.likes);
    if(!post.photo.empty())
    {
        QByteArray bar = QByteArray::fromStdString(post.photo) ;
        QPixmap pixmap;
        pixmap.loadFromData(bar);
        int w = ui->photo->width();
        int h = ui->photo->height();
        ui->photo->setPixmap(pixmap.scaled(w, h, Qt::KeepAspectRatio));
    }
    else
    {
        ui->photo->clear();
    }

    image = QByteArray::fromStdString(post.photo) ;
}


void Add::on_add_image_clicked()
{
    QString filter = "Image files (*.png *.xpm *.jpg *.jpeg *.gif)";
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::ExistingFile);
    QString path = dialog.getOpenFileName(
                this,
                "Select File", "", filter);
    if (path != "")
    {
        QFile file(path);
        if (file.open(QIODevice::ReadOnly))
        {
            image = file.readAll();
        }
    }
    if(image.size()!=0)
    {
        QPixmap pixmap;
        pixmap.loadFromData(image);
        int w = ui->photo->width();
        int h = ui->photo->height();
        ui->photo->setPixmap(pixmap.scaled(w, h, Qt::KeepAspectRatio));
    }
}



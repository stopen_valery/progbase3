#include "remotestorage.h"
#include "rpc/this_session.h"
#include "rpc/client.h"

using namespace std;

remotestorage::remotestorage(const string &dir_name, const string &addr, int port) : Storage(dir_name) {

    temp_client = new rpc::client(addr, port);

}

void remotestorage::checkConnection() {

    rpc::client::connection_state cs = temp_client->get_connection_state();

    if(cs != rpc::client::connection_state::connected) {

        qDebug() << "Server is not connected";
        throw "Error";

    }

}

remotestorage::~remotestorage() {

    rpc::this_session().post_exit();
    delete temp_client;

}
bool remotestorage::isOpen() const {

    return true;

}

bool remotestorage::open() {

    return true;

}

void remotestorage::close() {}

// users

vector<User> remotestorage::getAllUsers() {

    checkConnection();
    auto res = temp_client->async_call("getAllUsers");
    res.wait();
    auto result = res.get().as<vector<User>>();

    return result;

}

User remotestorage::getUserById(int user_id) {

    checkConnection();
    auto res = temp_client->async_call("getUserById", user_id);
    res.wait();
    auto result = res.get().as<User>();

    return result;

}

bool remotestorage::updateUser(const User &user) {

    checkConnection();
    auto res = temp_client->async_call("updateUser", user);
    res.wait();
    auto result = res.get().as<bool>();

    return result;

}

bool remotestorage::removeUser(int user_id) {

    checkConnection();
    auto res = temp_client->async_call("removeUser", user_id);
    res.wait();
    auto result = res.get().as<bool>();

    return result;

}

int remotestorage::insertUser(const User &user) {

    checkConnection();
    auto res = temp_client->async_call("insertUser", user);
    res.wait();
    auto result = res.get().as<int>();

    return result;

}

vector<User> remotestorage::pagination_user(std::string search, int page_num, int user_id, int f_followers, int t_followers, int  f_following, int t_following, int sort) {

    checkConnection();
    auto res = temp_client->async_call("pagination_user", search, page_num, user_id, f_followers, t_followers, f_following, t_following, sort);
    res.wait();
    auto result = res.get().as<vector<User>>();

    return result;

}

int remotestorage::get_num (std::string search, int user_id, int f_followers, int t_followers, int  f_following, int t_following){

    checkConnection();
    auto res = temp_client->async_call("get_num", search, user_id, f_followers, t_followers, f_following, t_following);
    res.wait();
    auto result = res.get().as<int>();

    return result;

}

vector<Post> remotestorage::getAllPosts() {

    checkConnection();
    auto res = temp_client->async_call("getAllPosts");
    res.wait();
    auto result = res.get().as<vector<Post>>();

    return result;

}

Post remotestorage::getPostById(int post_id) {

    checkConnection();
    auto res = temp_client->async_call("getPostById", post_id);
    res.wait();
    auto result = res.get().as<Post>();

    return result;

}

bool remotestorage::updatePost(const Post &post) {

    checkConnection();
    auto res = temp_client->async_call("updatePost", post);
    res.wait();
    auto result = res.get().as<bool>();

    return result;

}

bool remotestorage::removePost(int post_id) {

    checkConnection();
    auto res = temp_client->async_call("removePost", post_id);
    res.wait();
    auto result = res.get().as<bool>();

    return result;

}

int remotestorage::insertPost(const Post &post) {

    checkConnection();
    auto res = temp_client->async_call("insertPost", post);
    res.wait();
    auto result = res.get().as<int>();

    return result;

}

int remotestorage::insertDBUser(const DB_User &user) {

    checkConnection();
    auto res = temp_client->async_call("insertDBUser", user);
    res.wait();
    auto result = res.get().as<int>();

    return result;

}

DB_User remotestorage::getUserAuth(const string & username, const string & password) {

    checkConnection();
    auto res = temp_client->async_call("getUserAuth", username, password);
    res.wait();
    auto result = res.get().as<DB_User>();

    return result;

}

DB_User remotestorage::find_user_by_name(const string & username) {

    checkConnection();
    auto res = temp_client->async_call("find_user_by_name", username);
    res.wait();
    auto result = res.get().as<DB_User>();

    return result;

}

vector <User> remotestorage::getAllUserUsers(int db_user_id) {

    checkConnection();
    auto res = temp_client->async_call("getAllUserUsers", db_user_id);
    res.wait();
    auto result = res.get().as<vector<User>>();

    return result;

}

bool remotestorage::insertDBUserId(int db_user_id, int user_id) {

    checkConnection();
    auto res = temp_client->async_call("insertDBUserId", db_user_id, user_id);
    res.wait();
    auto result = res.get().as<bool>();

    return result;

}

vector <Post> remotestorage::getAllUserPosts(int user_id) {

    checkConnection();
    auto res = temp_client->async_call("getAllUserPosts", user_id);
    res.wait();
    auto result = res.get().as<vector<Post>>();

    return result;

}

bool remotestorage::insertUserPost(int user_id, int post_id) {

    checkConnection();
    auto res = temp_client->async_call("insertUserPost",user_id, post_id);
    res.wait();
    auto result = res.get().as<bool>();

    return result;

}

bool remotestorage::removeUserPost(int user_id, int post_id) {

    checkConnection();
    auto res = temp_client->async_call("removeUserPost",user_id, post_id);
    res.wait();
    auto result = res.get().as<bool>();

    return result;

}

vector <Post> remotestorage::searchPost(int user_id, std::string search, int f_likes, int t_likes, int f_comments, int t_comments) {

    checkConnection();
    auto res = temp_client->async_call("searchPost", user_id, search, f_likes, t_likes, f_comments, t_comments);
    res.wait();
    auto result = res.get().as<vector<Post>>();

    return result;

}

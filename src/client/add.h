
#ifndef ADD_H
#define ADD_H

#include <QDialog>
#include <QFileDialog>
#include <QBitArray>


#include "user.h"
#include "post.h"

namespace Ui {
class Add;
}

class Add : public QDialog
{
    Q_OBJECT

public:
    explicit Add(QWidget *parent = 0, bool entity=0);
    ~Add();
    User user_data() ;
    Post post_data() ;
    void user_value (const User &user) ;
    void post_value (const Post &post) ;

private slots:
    void on_add_image_clicked();


private:
    Ui::Add *ui;
    QByteArray image ;

};
#endif // ADD_H

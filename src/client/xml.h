#ifndef XML_H
#define XML_H

#include "remotestorage.h"
#include <fstream>


void Export (Storage *storage_, fstream &users_file_, vector <User> users) ;

void Import (Storage *storage_,fstream &users_file_, int db_user_ID) ;

#endif // XML_H

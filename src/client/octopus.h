#ifndef OCTOPUS_H
#define OCTOPUS_H

#include <QDialog>
#include <QFileDialog>
#include <QMessageBox>
#include "authentication.h"
#include "remotestorage.h"
#include "about.h"

namespace Ui {
class Octopus;
}

class Octopus : public QDialog
{
    Q_OBJECT

public:
    explicit Octopus(QWidget *parent = 0, Storage *DB =NULL);
    Storage *storage_ ;
    int db_user_id ;

    ~Octopus();

private slots:
    void on_LogIn_clicked();
    void wrong_auth() ;


    void on_About_clicked();

private:
    Ui::Octopus *ui;

};

#endif // OCTOPUS_H

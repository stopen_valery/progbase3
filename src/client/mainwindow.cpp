#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QEvent>
#include <QPaintEvent>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

        storage_ = new remotestorage{"../../data/sqlite", "127.0.0.1", 8080};

    try
    {
        storage_->open() ;
    }
    catch(char const * e)
    {
        QMessageBox::critical(this, tr("No Server"),
                              tr("Server is not connected\nCritical error"));
        exit(1) ;
    }
    ui->setupUi(this);
    connect(ui->actionLogOut, &QAction::triggered, this, &MainWindow::LogOut) ;
    connect(ui->actionExport_All_2, &QAction::triggered, this, &MainWindow::export_XML) ;
    connect(ui->actionExport_current_page_2, &QAction::triggered, this, &MainWindow::export_XML2) ;
    connect(ui->actionImport_XML_2, &QAction::triggered, this, &MainWindow::import_XML) ;
    connect(ui->actionAbout, &QAction::triggered, this, &MainWindow::about) ;

//    this->removeEventFilter(this);
//    this->update();
    open() ;
    this->setStyleSheet("QMessageBox {background-color: white; font:Times New Roman; font:12pt}");


}

MainWindow::~MainWindow()
{
    delete storage_ ;
    delete ui;
}

void MainWindow::LogOut()
{

    open() ;

}

void MainWindow::about()
{
    About about(this);
    about.setWindowTitle("About Octopus");
    about.exec() ;
}


void MainWindow::open()
{
      //this->setEnabled(false);
    this->resize(0, 0);

    Octopus oct (this,  storage_) ;
    oct.setWindowTitle("Welcome to Octopus");
    oct.exec() ;
    storage_ = oct.storage_ ;
    db_user_ID = oct.db_user_id ;
    if(db_user_ID == -1)
    {
        open() ;
    }
    else if (db_user_ID > 0)
    {
         //this->setEnabled(true) ;
        this->resize(1065, 567);
        unvisible() ;
        print_page(1);

    }
    if(db_user_ID == 0)
    {
        exit(0) ;
    }
    //qDebug() <<db_user_ID ;

}

void MainWindow::print_page (int page)
{
    QString s = ui->search_text->text();
    int t_followers = ui->t_followers->value()+2 ;
    int f_followers = ui->f_followers->value()-1 ;
    int t_following = ui->t_following->value()+2 ;
    int f_following = ui->f_following->value()-1 ;
    int sort = 0 ;
    if(ui->n_sort->isChecked())
    {
        sort = 1 ;
    }
    if(ui->p_sort->isChecked())
    {
        sort = 2;
    }
    vector<User> users ;
    try
    {
        users = storage_->pagination_user(s.toStdString(), page, db_user_ID, f_followers, t_followers, f_following, t_following, sort);
        ui->listWidget->clear();
    }
    catch(char const * e)
    {
        QMessageBox::critical(this, tr("No Server"),
                              tr("Server is not connected\nCritical error"));
        exit(1) ;

    }

    for(auto item = users.begin(); item !=users.end(); item++)
    {
        QListWidget * listWidget = ui->listWidget;
        QString inputText = QString::fromStdString(item->nick);
        QListWidgetItem * new_item = new QListWidgetItem(inputText);
        QVariant var = QVariant::fromValue((*item));
        new_item->setData(Qt::UserRole, var);
        listWidget->addItem(new_item);
    }

    try
    {
        all_users = storage_->get_num(s.toStdString(), db_user_ID, f_followers, t_followers, f_following, t_following) ;
    }
    catch(char const * e)
    {
        QMessageBox::critical(this, tr("No Server"),
                              tr("Server is not connected\nCritical error"));
        exit(1) ;

    }
    int pages=ceil((all_users  - 1) /10) + 1;
    ui->all_page->setText(QString::number(pages));

    ui->page->setText(QString::number(page));
    if(page == 1)
    {
        ui->prev->setEnabled(false);
    }
    else
    {
        ui->prev->setEnabled(true);
    }
    if(page>=ui->all_page->text().toInt())
    {
        ui->next->setEnabled(false);
    }
    else
    {
        ui->next->setEnabled(true);
    }

}


void MainWindow::print_post()
{
    string search = ui->search_text_2->text().toStdString() ;
    int f_likes = ui->f_likes->value()-1;
    int t_likes = ui->t_likes->value()+1;
    int f_coms = ui->f_coms->value()-1;
    int t_coms = ui->t_coms->value()+1;
    vector <Post> posts = storage_->searchPost(user_ID, search, f_likes, t_likes, f_coms, t_coms) ;
    ui->add_2->setEnabled(true);
    ui->delete_3->setEnabled(true);
    ui->search_text_2->setEnabled(true) ;
    ui->search_text_2->setEnabled(true) ;
    ui->f_coms->setEnabled(true);
    ui->t_coms->setEnabled(true);
    ui->t_likes->setEnabled(true);
    ui->f_likes->setEnabled(true);
    if(!posts.empty())
    {
        ui->edit_2->setEnabled(true);

    }
    else
    {
        ui->edit_2->setEnabled(false);
    }

    ui->listWidget_2->clear();
    for(auto item = posts.begin(); item !=posts.end(); item++)
    {
        QListWidget * listWidget = ui->listWidget_2;
        QString inputText = QString::fromStdString(item->place);
        QListWidgetItem * new_item = new QListWidgetItem(inputText);
        QVariant var = QVariant::fromValue((*item));
        new_item->setData(Qt::UserRole, var);
        listWidget->addItem(new_item);
    }
}

void MainWindow::export_XML()
{

    QFileDialog dialog(this) ;
    dialog.setFileMode(QFileDialog::Directory);
    QString current_dir = QDir::currentPath();
    QString default_name = "my_users";
    QString folder_path = dialog.getSaveFileName(this, "Select Export Folder", current_dir + "/" + default_name, "Folders");
    fstream users_file_ ;
    users_file_.open(folder_path.toStdString()+".xml", std::ios::out);
    vector<User> users = storage_->getAllUserUsers(db_user_ID);
    Export(storage_, users_file_, users) ;

    if(!folder_path.isEmpty())
    {
        QMessageBox msgBox ;
        msgBox.setWindowTitle("Export");
        msgBox.setStyleSheet("background-color: white; color:black; font:Times New Roman; font:12pt");
        msgBox.setText("Data exported successfully");
        msgBox.exec();
    }
}

void MainWindow::export_XML2()
{

    QFileDialog dialog(this) ;
    dialog.setFileMode(QFileDialog::Directory);
    QString current_dir = QDir::currentPath();
    QString default_name = "my_users";
    QString folder_path = dialog.getSaveFileName(this, "Select Export Folder", current_dir + "/" + default_name, "Folders");
    fstream users_file_ ;
    users_file_.open(folder_path.toStdString()+".xml", std::ios::out);
    QString s = ui->search_text->text();
    int t_followers = ui->t_followers->value()+2 ;
    int f_followers = ui->f_followers->value()-1 ;
    int t_following = ui->t_following->value()+2 ;
    int f_following = ui->f_following->value()-1 ;
    int page = ui->page->text().toInt() ;
    int sort = 0 ;
    if(ui->n_sort->isChecked())
    {
        sort = 1 ;
    }
    if(ui->p_sort->isChecked())
    {
        sort = 2;
    }

    vector<User> users = storage_->pagination_user(s.toStdString(), page, db_user_ID, f_followers, t_followers, f_following, t_following, sort);
    Export(storage_, users_file_, users) ;

    if(!folder_path.isEmpty())
    {
        QMessageBox msgBox ;
        msgBox.setWindowTitle("Export");
        msgBox.setStyleSheet("background-color: white; color:black; font:Times New Roman; font:12pt");
        msgBox.setText("Data exported successfully");
        msgBox.exec();
    }
}


void MainWindow::import_XML()
{
    QString filter = "Xml files (*.xml)";
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::ExistingFile);
    QString folder_path = dialog.getOpenFileName(this,"Select File", "", filter);
    fstream users_file_ ;
    users_file_.open(folder_path.toStdString(), std::ios::in);

    Import(storage_,users_file_, db_user_ID) ;

    if(!folder_path.isEmpty())
    {
        QMessageBox msgBox ;
        msgBox.setWindowTitle("Export");
        msgBox.setStyleSheet("background-color: white; color:black; font:Times New Roman; font:12pt");
        msgBox.setText("Data exported successfully");
        msgBox.exec();

        print_page(1);
    }
}


void MainWindow::on_listWidget_itemClicked(QListWidgetItem *item)
{
    ui->edit->setEnabled(true);
    QVariant var = item->data(Qt::UserRole) ;
    User us = var.value<User>() ;
    ui->Username->setText("Nickname:") ;
    ui->username->setText(QString::fromStdString(us.nick)) ;
    ui->Followers->setText("Followers:");
    ui->followers->setText(QString::number(us.followers)) ;
    ui->Following->setText("Following:");
    ui->following->setText(QString::number(us.following));
    ui->Bio->setText("Bio:");
    ui->bio->setText(QString::fromStdString(us.bio));
    ui->us_info->setText("Users`s Info:");
    ui->usposts->setText("Users`s Posts:");
    if(!us.photo.empty())
    {
        QByteArray bar = QByteArray::fromStdString(us.photo) ;
        QPixmap pixmap;
        pixmap.loadFromData(bar);
        int w = ui->photo->width();
        int h = ui->photo->height();
        ui->photo->setPixmap(pixmap.scaled(w, h, Qt::KeepAspectRatio));
    }
    else
    {
        QPixmap pixmap ("../../pictures/octopus_ava.jpg") ;
        int w = ui->photo->width();
        int h = ui->photo->height();
        ui->photo->setPixmap(pixmap.scaled(w, h, Qt::KeepAspectRatio));
    }
    user_ID = us.ID ;

    print_post();

}

bool  MainWindow::check_input(std::string name)
{
    if(name.empty())
    {
        QMessageBox msgBox ;
        msgBox.setWindowTitle("Empty field");
        msgBox.setStyleSheet("background-color: white; color:black; font:Times New Roman; font:12pt");
        msgBox.setText("Oopsie!\nThis field can not be empty\nTry again");
        msgBox.exec();
        return false ;
    }
    return true ;
}

void MainWindow::on_add_clicked()
{
    if (storage_ != nullptr)
    {
        Add add(this, false);
        add.setWindowTitle("Add User");
        int status = add.exec();

        if (status == 1)
        {

            User user = add.user_data();
            while(check_input(user.nick)==false)
            {
                //add(this, false);
                add.setWindowTitle("Add User");
                int status = add.exec();
                if (status == 1)
                {
                    user = add.user_data();
                }
                else
                {
                    break ;
                }
            }
            if(!user.nick.empty())
            {

                try
                {
                    user.ID = storage_->insertUser(user);
                    storage_->insertDBUserId(db_user_ID, user.ID) ;
                }
                catch(char const * e)
                {
                    QMessageBox::critical(this, tr("No Server"),
                                          tr("Server is not connected\nCritical error"));
                    exit(1) ;

                }
                QString inputText = QString::fromStdString(user.nick);
                QListWidgetItem * new_item = new QListWidgetItem(inputText);
                QVariant var = QVariant::fromValue(user);
                new_item->setData(Qt::UserRole, var);
                print_page(ui->page->text().toInt());
                QString id_str = "New user's id: " + QString::number(user.ID);
                QMessageBox msgBox ;
                msgBox.setWindowTitle("Adding");
                msgBox.setStyleSheet("background-color: white; color:black; font:Times New Roman; font:12pt");
                msgBox.setText(id_str);
                msgBox.exec();
            }

        }

    }
}

void MainWindow::unvisible()
{
    ui->username->clear();
    ui->Username->clear();
    ui->bio->clear();
    ui->Bio->clear();
    ui->followers->clear();
    ui->Followers->clear();
    ui->following->clear() ;
    ui->Following->clear();
    ui->photo->clear() ;
    ui->us_info->clear();
    ui->usposts->clear() ;
    ui->listWidget_2->clear();
    ui->add_2->setEnabled(false);
    ui->delete_3->setEnabled(false);
    ui->edit_2->setEnabled(false);
    ui->edit->setEnabled(false);
    ui->search_text_2->setEnabled(false) ;
    ui->f_coms->setEnabled(false);
    ui->t_coms->setEnabled(false);
    ui->t_likes->setEnabled(false);
    ui->f_likes->setEnabled(false);
}

void MainWindow::on_delete_2_clicked()
{
    QList<QListWidgetItem *> list = ui->listWidget->selectedItems();
    if (list.count() > 0)
    {
        QMessageBox::StandardButton reply;
        reply = QMessageBox::question(
                    this,
                    "Delete User",
                    "Are you sure?",
                    QMessageBox::Yes|QMessageBox::No);
        if (reply == QMessageBox::Yes)
        {

            QListWidgetItem * selectedItem = list.at(0);
            int itemRow = ui->listWidget->row(selectedItem);
            ui->listWidget->takeItem(itemRow);

            QVariant var = selectedItem->data(Qt::UserRole);
            User tmp = var.value<User>();
            vector <Post>  posts ;
            try
            {
               posts = storage_->getAllUserPosts(tmp.ID) ;
            }
            catch(char const * e)
            {
                QMessageBox::critical(this, tr("No Server"),
                                      tr("Server is not connected\nCritical error"));
                exit(1) ;

            }
            if (!posts.empty())
            {
                for (Post po : posts)
                {
                    try
                    {
                        storage_->removeUserPost(tmp.ID, po.ID) ;
                        storage_->removePost(po.ID) ;

                    }
                    catch(char const * e)
                    {
                        QMessageBox::critical(this, tr("No Server"),
                                              tr("Server is not connected\nCritical error"));
                        exit(1) ;

                    }
                }
            }

            try
            {
                storage_->removeUser(tmp.ID) ;
            }
            catch(char const * e)
            {
                QMessageBox::critical(this, tr("No Server"),
                                      tr("Server is not connected\nCritical error"));
                exit(1) ;

            }

            unvisible();

            delete selectedItem;
            print_page(ui->page->text().toInt());
        }
    }
}

void MainWindow::on_edit_clicked()
{
    QList<QListWidgetItem *> items = ui->listWidget->selectedItems();

    if(items.count() == 1)
    {
        QListWidgetItem * selectedItem = items.at(0);
        QVariant var = selectedItem->data(Qt::UserRole);
        User tmp = var.value<User>();
        Add edit(this, false);
        edit.setWindowTitle("Edit User");
        edit.user_value(tmp);
        int status = edit.exec();
        if (status == 1)
        {
            User user = edit.user_data();
            while(check_input(user.nick)==false)
            {
                edit.setWindowTitle("Edit User");
                int status = edit.exec();
                if (status == 1)
                {
                    user = edit.user_data();
                }
                else
                {
                    break ;
                }
            }
            if(!user.nick.empty())
            {
                user.ID = tmp.ID;
                try
                {
                storage_->updateUser(user);
                }
                catch(char const * e)
                {
                    QMessageBox::critical(this, tr("No Server"),
                                                   tr("Server is not connected\nCritical error"));
                    exit(1) ;

                }
                QMessageBox msgBox ;
                msgBox.setWindowTitle("Editing");
                msgBox.setStyleSheet("background-color: white; color:black; font:Times New Roman; font:12pt");
                msgBox.setText("User edited successfully");
                msgBox.exec();
                selectedItem->setText(QString::fromStdString(user.nick));
                QVariant tmpvar = QVariant::fromValue(user);
                selectedItem->setData(Qt::UserRole, tmpvar);
                on_listWidget_itemClicked(selectedItem);
            }

        }
    }
}


void MainWindow::on_search_text_textChanged()
{
    unvisible() ;
    print_page(1);

}

void MainWindow::on_next_clicked()
{
    int page = ui->page->text().toInt()+1 ;
    print_page(page);
}

void MainWindow::on_prev_clicked()
{
    int page = ui->page->text().toInt()-1 ;
    print_page(page);
}

void MainWindow::on_add_2_clicked()
{
    if (storage_ != nullptr)
    {
        Add add(this, true);
        add.setWindowTitle("Add Post");
        int status = add.exec();

        if (status == 1)
        {

            Post post = add.post_data();
            while(check_input(post.place)==false)
            {
                add.setWindowTitle("Add Post");
                int status = add.exec();
                if (status == 1)
                {
                    post = add.post_data();
                }
                else
                {
                    break ;
                }
            }
            if(!post.place.empty())
            {
                try
                {
                post.ID = storage_->insertPost(post);
                }
                catch(char const * e)
                {
                    QMessageBox::critical(this, tr("No Server"),
                                                   tr("Server is not connected\nCritical error"));
                    exit(1) ;

                }
                try
                {
                storage_->insertUserPost(user_ID, post.ID) ;
                }
                catch(char const * e)
                {
                    QMessageBox::critical(this, tr("No Server"),
                                                   tr("Server is not connected\nCritical error"));
                    exit(1) ;

                }
                QString inputText = QString::fromStdString(post.place);
                QListWidgetItem * new_item = new QListWidgetItem(inputText);
                QVariant var = QVariant::fromValue(post);
                print_post();
                new_item->setData(Qt::UserRole, var);
                QString id_str = "New post's id: " + QString::number(post.ID);
                QMessageBox::information(this, "Added", id_str);
            }

        }

    }
}

void MainWindow::on_delete_3_clicked()
{
    QList<QListWidgetItem *> list = ui->listWidget_2->selectedItems();
    if (list.count() > 0)
    {
        QMessageBox::StandardButton reply;
        reply = QMessageBox::question(
                    this,
                    "Delete Post",
                    "Are you sure?",
                    QMessageBox::Yes|QMessageBox::No);
        if (reply == QMessageBox::Yes)
        {

            QListWidgetItem * selectedItem = list.at(0);
            int itemRow = ui->listWidget->row(selectedItem);
            ui->listWidget->takeItem(itemRow);

            QVariant var = selectedItem->data(Qt::UserRole);
            Post tmp = var.value<Post>();

            try
            {
            storage_->removeUserPost(user_ID, tmp.ID) ;
            }
            catch(char const * e)
            {
                QMessageBox::critical(this, tr("No Server"),
                                               tr("Server is not connected\nCritical error"));
                exit(1) ;

            }
            try
            {
            storage_->removePost(tmp.ID) ;
            }
            catch(char const * e)
            {
                QMessageBox::critical(this, tr("No Server"),
                                               tr("Server is not connected\nCritical error"));
                exit(1) ;

            }
            delete selectedItem;
            print_post();

        }

    }
}

void MainWindow::on_edit_2_clicked()
{
    QList<QListWidgetItem *> items = ui->listWidget_2->selectedItems();

    if(items.count() == 1)
    {
        QListWidgetItem * selectedItem = items.at(0);
        QVariant var = selectedItem->data(Qt::UserRole);
        Post tmp = var.value<Post>();
        Add edit(this, true);
        edit.setWindowTitle("Edit Post");
        edit.post_value(tmp);
        int status = edit.exec();
        if (status == 1)
        {
            Post post = edit.post_data();
            while(check_input(post.place)==false)
            {
                edit.setWindowTitle("Edit Post");
                int status = edit.exec();
                if (status == 1)
                {
                    post = edit.post_data();
                }
                else
                {
                    break ;
                }
            }
            if(!post.place.empty())
            {
                post.ID = tmp.ID;
                try
                {
                storage_->updatePost(post);
                }
                catch(char const * e)
                {
                    QMessageBox::critical(this, tr("No Server"),
                                                   tr("Server is not connected\nCritical error"));
                    exit(1) ;

                }
                QMessageBox msgBox ;
                msgBox.setWindowTitle("Editing");
                msgBox.setStyleSheet("background-color: white; color:black; font:Times New Roman; font:12pt");
                msgBox.setText("Post edited successfully");
                msgBox.exec();
                selectedItem->setText(QString::fromStdString(post.place));
                QVariant tmpvar = QVariant::fromValue(post);
                selectedItem->setData(Qt::UserRole, tmpvar);
            }
        }
    }
}

void MainWindow::on_search_text_2_textChanged()
{
    print_post();
}

void MainWindow::on_f_followers_valueChanged(int arg1)
{
    print_page(1);
}

void MainWindow::on_t_followers_valueChanged(int arg1)
{
    print_page(1);
}

void MainWindow::on_f_following_valueChanged(int arg1)
{
    print_page(1);
}

void MainWindow::on_t_following_valueChanged(int arg1)
{
    print_page(1);
}

void MainWindow::on_f_likes_valueChanged(int arg1)
{
    print_post();
}


void MainWindow::on_t_likes_valueChanged(int arg1)
{
    print_post();
}

void MainWindow::on_f_coms_valueChanged(int arg1)
{
    print_post();
}

void MainWindow::on_t_coms_valueChanged(int arg1)
{
    print_post();
}

void MainWindow::on_n_sort_clicked()
{
    if(ui->p_sort->isChecked())
    {
        ui->p_sort->setChecked(false);
    }
    print_page(1) ;
}

void MainWindow::on_p_sort_clicked()
{
    if(ui->n_sort->isChecked())
    {
        ui->n_sort->setChecked(false);
    }
    print_page(1) ;
}

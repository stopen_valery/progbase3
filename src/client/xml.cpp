#include <QtXml>
#include "xml.h"

void Export(Storage *storage_, fstream &users_file_, vector<User> users)
{
    QDomDocument doc;
    QDomElement rootEl = doc.createElement("users");
    string str ;
    for (const User & user: users)
    {
        QDomElement temp = doc.createElement("user") ;
        int ID = user.ID ;
        vector <Post> posts = storage_->getAllUserPosts(user.ID) ;
        for (const Post & post: posts)
        {
            QDomElement temp2 = doc.createElement("post") ;
            int ID = post.ID ;
            std::string description = post.description ;
            std::string place = post.place ;
            int likes = post.likes ;
            int comments = post.comments ;

            temp2.setAttribute("ID", QString::number(ID));
            temp2.setAttribute("description",QString::fromStdString(description));
            temp2.setAttribute("place",QString::fromStdString(place));
            temp2.setAttribute("likes", QString::number(likes));
            temp2.setAttribute("comments", QString::number(comments));

            temp.appendChild(temp2);
        }
        std::string nick = user.nick ;
        int followers = user.followers ;
        int following = user.following ;
        std::string bio = user.bio ;

        temp.setAttribute("ID", QString::number(ID));
        temp.setAttribute("nick",QString::fromStdString(nick));
        temp.setAttribute("followers", QString::number(followers));
        temp.setAttribute("following", QString::number(following));
        temp.setAttribute("bio",QString::fromStdString(bio));

        rootEl.appendChild(temp);
    }
    doc.appendChild(rootEl);

    QString qStr = doc.toString(5);
    str = qStr.toStdString(); ;

    users_file_ << str;
    users_file_.close();

}

void Import (Storage *storage_, fstream &users_file_,  int db_user_ID)
{
    if(!users_file_.is_open())
    {qDebug() << "CLOSE" ;
        exit(1);}
    std::string temp;
    std::string xml_string;
    while (!users_file_.eof())
    {
        getline(users_file_, temp = "");
        xml_string += temp;
        xml_string += '\n';
    }
    if(xml_string.empty() || xml_string=="\n")
    {
        qDebug() <<"The file is empty\n" ;
    }
    xml_string.pop_back();
    //qDebug() <<QString::fromStdString(xml_string) ;
    QString xmlstr = QString::fromStdString(xml_string) ;
    vector <User> users ;
    QDomDocument doc ;
    QString errorMessage;
    if (!doc.setContent(xmlstr, &errorMessage))
    {

        qDebug() << "error parsing xml: " << errorMessage;
        exit(1);

    }
    QDomElement rootEl = doc.documentElement();
    QDomNodeList rootElChildren = rootEl.childNodes();
    for (int i = 0; i < rootElChildren.length(); i++) {

        QDomNode userNode = rootElChildren.at(i);
        QDomElement userEl = userNode.toElement();

        User user;
        //QString ID = userEl.attribute("ID") ;
        QString nick = userEl.attribute("nick") ;
        QString followers = userEl.attribute("followers") ;
        QString following = userEl.attribute("following") ;
        QString bio = userEl.attribute("bio") ;

        //user.ID = ID.toInt() ;
        user.nick = nick.toStdString() ;
        user.followers = followers.toInt() ;
        user.following = following.toInt();
        user.bio = bio.toStdString();

        int uID = storage_->insertUser(user) ;
        storage_->insertDBUserId(db_user_ID, uID) ;


        QDomNodeList rootUsChildren = userEl.childNodes() ;
        for (int j = 0; j < rootUsChildren.length(); j++)
        {
            QDomNode postNode = rootUsChildren.at(j);
            QDomElement postEl = postNode.toElement();

            Post post;
            QString description = postEl.attribute("description") ;
            QString place = postEl.attribute("place") ;
            QString likes = postEl.attribute("likes") ;
            QString comments = postEl.attribute("comments") ;

            post.description = description.toStdString() ;
            post.place = place.toStdString() ;
            post.likes = likes.toInt();
            post.comments = comments.toInt();

            int pID = storage_->insertPost(post);
            storage_->insertUserPost(uID, pID) ;


        }

    }

    users_file_.close();
}


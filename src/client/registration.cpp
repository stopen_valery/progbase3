#include "registration.h"
#include "ui_registration.h"

Registration::Registration(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Registration)
{
        ui->setupUi(this);
}

Registration::~Registration()
{
    delete ui;
}

void Registration::set_data (Storage * DB)
{
    this->storage_ = DB ;
}

bool Registration::check_pass()
{
    QString pass1 = ui->password->text() ;
    QString pass2 = ui->password2->text();
    if(pass1!=pass2)
    {
        ui->password->clear();
        ui->password2->clear();
        QMessageBox msgBox ;
        msgBox.setWindowTitle("Different passwords");
        msgBox.setStyleSheet("background-color: white; color:black; font:Times New Roman; font:12pt");
        msgBox.setText("Oopsie!\nYour passwords are different\nTry again");
        msgBox.exec();
        return false ;
    }
    if(pass1.size()<=5)
    {
        ui->password->clear();
        ui->password2->clear();
        QMessageBox msgBox ;
        msgBox.setWindowTitle("Short password");
        msgBox.setStyleSheet("background-color: white; color:black; font:Times New Roman; font:12pt");
        msgBox.setText("Oopsie!\nThe password is too short\nInput more than 5 symbols for your security");
        msgBox.exec();
        return false ;
    }
    return true ;
}

bool Registration::check_name ()
{
    string username = ui->username->text().toStdString() ;
    int id = 0;
    try
    {
         id = storage_->find_user_by_name(username).ID ;
    }
    catch(char const * e)
    {
        QMessageBox::critical(this, tr("No Server"),
                                       tr("Server is not connected\nCritical error"));
        exit(1) ;

    }
    qDebug() << id ;
    if(id > 0)
    {
        ui->username->clear();
        QMessageBox msgBox ;
        msgBox.setWindowTitle("Existing User");
        msgBox.setStyleSheet("background-color: white; color:black; font:Times New Roman; font:12pt");
        msgBox.setText("Oopsie!\nSuch username already exists\nTry again");
        msgBox.exec();
        return false ;
    }
    if(username.size()<2)
    {
        ui->username->clear();
        QMessageBox msgBox ;
        msgBox.setWindowTitle("Short Username");
        msgBox.setStyleSheet("background-color: white; color:black; font:Times New Roman; font:12pt");
        msgBox.setText("Oopsie!\nThe username is too short\nInput more than 2 symbols");
        msgBox.exec();
        return false ;
    }
    return true ;
}

int Registration::get_user_ID()
{
    return DB_user_ID ;
}

void Registration::on_ok_clicked()
{

    DB_User user ;
    DB_user_ID = -1 ;
    if(check_name() == true)
    {
        if(check_pass() == true)
        {
            string username = ui->username->text().toStdString() ;
            user.username = username ;
            QString password = ui->password->text() ;
            user.password_hash = HashPassword(password).toStdString() ;
            try{
            DB_user_ID = storage_->insertDBUser(user) ;
            }
            catch(char const * e)
            {
                QMessageBox::critical(this, tr("No Server"),
                                               tr("Server is not connected\nCritical error"));
                exit(1) ;

            }
            this->close () ;
        }
    }

}



void Registration::on_cancel_clicked()
{
    this->close() ;
}

#include "authentication.h"
#include "ui_authentication.h"
#include <QCloseEvent>

using namespace std ;



Authentication::Authentication(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Authentication)
{ DB_User user;
    ui->setupUi(this);
    DB_user_ID = -1 ;
    this->setStyleSheet("QMessageBox {background-color: white;  font: Times New Roman, 12 pt}");
}

Authentication::~Authentication()
{
    delete ui;
}

QString HashPassword(QString const & pass)
{
    QByteArray pass_ba = pass.toUtf8();
    QByteArray hash_ba = QCryptographicHash::hash(pass_ba, QCryptographicHash::Md5);
    QString pass_hash = QString(hash_ba.toHex());
    return pass_hash;
}

int Authentication::get_DB_user_ID()
{
    return DB_user_ID ;
}

void Authentication::set_data (Storage * DB)
{
    this->storage_ = DB ;
}

void Authentication::on_ok_clicked()
{
    DB_user_ID = -1 ;
    string username = ui->username->text().toStdString() ;
    QString password = ui->password->text() ;
    password = HashPassword(password) ;
    DB_User  user ;
    try
    {
    user = storage_->getUserAuth(username, password.toStdString()) ;
    }
    catch(char const * e)
    {
        QMessageBox::critical(this, tr("No Server"),
                                       tr("Server is not connected\nCritical error"));
        exit(1) ;

    }
    if(user.ID != -1)
    {
        DB_user_ID = user.ID ;
    }
    else
    {
        DB_user_ID = -2 ;
    }

    this->close() ;
}



void Authentication::on_cancel_clicked()
{
    DB_user_ID = -1 ;
    this->close() ;
}

void Authentication::on_Registrate_clicked()
{
    Registration reg(this);
    reg.setWindowTitle("Registration");
    DB_User user ;
    reg.set_data(this->storage_) ;
    reg.exec();
    DB_user_ID = reg.get_user_ID() ;
    if(DB_user_ID != -1)
    {
        this->close() ;
    }
}


void Authentication::closeEvent (QCloseEvent *event)
{
    if(DB_user_ID==-1)
    {
        QMessageBox::StandardButton resBtn = QMessageBox::question( this,"Close",
                                                                    tr("All the information will be lost\nAre you sure?\n"),
                                                                    QMessageBox::No | QMessageBox::Yes,
                                                                    QMessageBox::No);

        if (resBtn != QMessageBox::Yes) {
            event->ignore();
        } else
        {
            DB_user_ID = -1 ;
            event->accept();
        }
    }

}

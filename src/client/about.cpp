#include "about.h"
#include "ui_about.h"

#include <QMovie>

About::About(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::About)
{
    ui->setupUi(this);
    hide(false) ;
    movie=new QMovie("../../pictures/about.gif");
    ui->oct->setMovie(movie);

    connect(movie, &QMovie::frameChanged, [=](int frameNumber){
        if(frameNumber == movie->frameCount()-1){
            movie->stop();
            delete(movie) ;
            movie = NULL ;
            on_aboutB_clicked();
        }
    });
    movie->start();

    ui->donat->setTextInteractionFlags(Qt::TextBrowserInteraction);


}


About::~About()
{
    delete ui;
    if(movie!=NULL)
    {
        delete(movie) ;
    }
}

void About::hide(bool h)
{
    ui->label->setVisible(h);
    ui->textBrowser->setVisible(h);
    ui->aboutB->setVisible(h);
    ui->ConB->setVisible(h);
    ui->want->setVisible(h);
    ui->donat->setVisible(h);
}

void About::on_aboutB_clicked()
{


    hide(true) ;
    ui->aboutB->setEnabled(false) ;
    ui->ConB->setEnabled(true) ;
    ui->textBrowser->setText(    "   This program allows you to work with users and their posts, offering following commands:\n");
    ui->textBrowser->append("<b>User commands:<\b>") ;
    ui->textBrowser->append("1. Add User - press the central button to add user\n"
                            "2. Delete User - press the cental right button to delete user\n"
                            "3. Edit User- press the cental left button to edit user\n"
                            "4. Filter Users - filters located above users list\n"
                            "5. Sort Users - sort check-buttons located under users list (either by name, by popularity or by default\n"
                            "6. Search for Users - the search-line located above users list\n") ;
    ui->textBrowser->append("<b>Post commands:<\b>") ;

    ui->textBrowser->append("1. Add Post - press the button on the left of the window\n"
                            "2. Delete Post - press the button on the left of the window\n"
                            "3. Edit Post - press the button More on the left of the window\n"
                            "4. Filter Posts - filters located user posts list\n"
                            "5. Search for Posts- the search-line located under posts list\n") ;

    ui->textBrowser->append("<b>Other commands:<\b>") ;
    ui->textBrowser->append("1. Export XML (either all or one page) - Hot Key: Ctrl+E or Ctrl+Shift+E\n"
                            "2. Import XML - Hot Key: Ctrl+I\n"
                            "3. Log Out - Hot Key: Ctrl+L") ;

    ui->textBrowser->append("   If you do not have an account yet, you can Registrate clicking on Log In -> Registrate\n");
    ui->donat->setText("<a href=\"whatever\">Patreon</a>");



}

void About::on_ConB_clicked()
{

    hide(true) ;
    ui->aboutB->setEnabled(true) ;
    ui->ConB->setEnabled(false) ;
    ui->textBrowser->setText(    "   Have any guestions left?"
                                 "Contact us\n");
    ui->textBrowser->append("<b>Email:<\b>") ;
    ui->textBrowser->append("stopen.lera@gmail.com") ;
    ui->textBrowser->append("<b>Phone:<\b>") ;
    ui->textBrowser->append("+(38)095-431-79-77\n") ;
    ui->textBrowser->append("<i>Animation by Cinematic<\i>\n");
    ui->textBrowser->append("<i>Design by Valery<\i>\n") ;

}


void About::closeEvent (QCloseEvent *event)
{
    // movie->blockSignals(true) ;
}


void About::on_donat_linkActivated(const QString &link)
{
    QDesktopServices::openUrl(QUrl("https://www.patreon.com/user?u=36159608"));
}

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QAction>
#include <QDebug>
#include <QMessageBox>
#include <QFileDialog>
#include <QListWidgetItem>
#include <QVariant>
#include <QDialog>
#include <QByteArray>
#include <QPixmap>
#include <fstream>
#include <QtXml>

#include "remotestorage.h"
#include "authentication.h"
#include "octopus.h"
#include "add.h"
#include "xml.h"
#include "about.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    Storage *storage_ ;
    int db_user_ID ;
    int all_users ;
    int user_ID ;
    void unvisible() ;
    void LogOut() ;
    void export_XML() ;
    void export_XML2() ;
    void about() ;

    void import_XML() ;
    bool check_input(std::string name) ;

private slots:
    void print_page (int page) ;
    void open() ;
    void on_listWidget_itemClicked(QListWidgetItem *item);
    void on_add_clicked();

    void on_delete_2_clicked();

    void on_edit_clicked();
    void on_next_clicked();
    void on_prev_clicked();
    void print_post() ;
    void on_search_text_textChanged();
    void on_add_2_clicked();
    void on_delete_3_clicked();
    void on_edit_2_clicked();
    void on_search_text_2_textChanged();
    void on_f_followers_valueChanged(int arg1);
    void on_t_followers_valueChanged(int arg1);
    void on_f_following_valueChanged(int arg1);
    void on_t_following_valueChanged(int arg1);
    void on_f_likes_valueChanged(int arg1);
    void on_t_likes_valueChanged(int arg1);
    void on_f_coms_valueChanged(int arg1);
    void on_t_coms_valueChanged(int arg1);
    void on_n_sort_clicked();
    void on_p_sort_clicked();
};

#endif // MAINWINDOW_H

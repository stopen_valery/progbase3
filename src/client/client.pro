#-------------------------------------------------
#
# Project created by QtCreator 2020-05-20T12:14:30
#
#-------------------------------------------------

QT       += core gui
QT += sql
QT += xml
QT += core

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = client
TEMPLATE = app

QMAKE_CXXFLAGS += -std=c++17


SOURCES += main.cpp\
        mainwindow.cpp \
    registration.cpp \
    octopus.cpp \
    authentication.cpp \
    add.cpp \
    remotestorage.cpp \
    xml.cpp \
    about.cpp

HEADERS  += mainwindow.h \
    registration.h \
    octopus.h \
    authentication.h \
    add.h \
    remotestorage.h \
    xml.h \
    about.h

FORMS    += mainwindow.ui \
    registration.ui \
    octopus.ui \
    authentication.ui \
    add.ui \
    about.ui

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../storage/release/ -lstorage
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../storage/debug/ -lstorage
else:unix: LIBS += -L$$OUT_PWD/../storage/ -lstorage

INCLUDEPATH += $$PWD/../storage
DEPENDPATH += $$PWD/../storage

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../storage/release/libstorage.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../storage/debug/libstorage.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../storage/release/storage.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../storage/debug/storage.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../storage/libstorage.a

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../rpclib-master/release/ -lrpc
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../rpclib-master/debug/ -lrpc
else:unix: LIBS += -L$$PWD/../../../rpclib-master/ -lrpc

INCLUDEPATH += $$PWD/../../../rpclib-master
DEPENDPATH += $$PWD/../../../rpclib-master

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../rpclib-master/release/librpc.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../rpclib-master/debug/librpc.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../rpclib-master/release/rpc.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../rpclib-master/debug/rpc.lib
else:unix: PRE_TARGETDEPS += $$PWD/../../../rpclib-master/librpc.a

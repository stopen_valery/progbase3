#include "octopus.h"
#include "ui_octopus.h"

Octopus::Octopus(QWidget *parent, Storage *DB) :
    QDialog(parent),
    ui(new Ui::Octopus)
{
    ui->setupUi(this);
    db_user_id = 0;
    this->storage_ = DB ;
}

Octopus::~Octopus()
{
    delete ui ;
}

void Octopus::wrong_auth()
{
    Authentication auth(this);
    auth.setWindowTitle("Log In");
    auth.set_data(this->storage_) ;
    auth.exec() ;
    db_user_id = auth.get_DB_user_ID();
}

void Octopus::on_LogIn_clicked()
{
    db_user_id = -1 ;

    wrong_auth();
    while(db_user_id == -2)
    {
        QMessageBox msgBox ;
        msgBox.setWindowTitle("Wrong Input");
        msgBox.setStyleSheet("background-color: white; color:black; font:Times New Roman; font:12pt");
        msgBox.setText("Oopsie!\nThe username or password is wrong\nTry again");
        msgBox.exec();
        wrong_auth();
    }

        this->close();


}

void Octopus::on_About_clicked()
{
    About about(this);
    about.setWindowTitle("About Octopus");
    about.exec() ;
}

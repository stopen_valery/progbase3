#ifndef REGISTRATION_H
#define REGISTRATION_H

#include <QDialog>
#include <remotestorage.h>
#include "authentication.h"

namespace Ui {
class Registration;
}

class Registration : public QDialog
{
    Q_OBJECT

public:
    explicit Registration(QWidget *parent = 0);
    void set_data (Storage * DB) ;
    int get_user_ID() ;
    ~Registration();

private slots:
    void on_ok_clicked();

    void on_cancel_clicked();

private:
    Ui::Registration *ui;
    Storage *storage_ ;
    int DB_user_ID ;
    bool check_pass() ;
    bool check_name () ;
};

#endif // REGISTRATION_H

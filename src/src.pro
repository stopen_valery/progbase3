TEMPLATE = subdirs

SUBDIRS += \
    client \
    server \
    storage

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../rpclib-master/release/ -lrpc
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../rpclib-master/debug/ -lrpc
else:unix: LIBS += -L$$PWD/../../rpclib-master/ -lrpc

INCLUDEPATH += $$PWD/../../rpclib-master
DEPENDPATH += $$PWD/../../rpclib-master

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../rpclib-master/release/librpc.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../rpclib-master/debug/librpc.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../rpclib-master/release/rpc.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../rpclib-master/debug/rpc.lib
else:unix: PRE_TARGETDEPS += $$PWD/../../rpclib-master/librpc.a

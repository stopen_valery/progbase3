QT += core
QT -= gui

QT += sql
QT += xml
QT += core

TARGET = server

CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

QMAKE_CXXFLAGS += -std=c++17

SOURCES += main.cpp \
    sqlitestorage.cpp \
    remoteui.cpp

HEADERS += \
    sqlitestorage.h \
    remoteui.h


win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../storage/release/ -lstorage
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../storage/debug/ -lstorage
else:unix: LIBS += -L$$OUT_PWD/../storage/ -lstorage

INCLUDEPATH += $$PWD/../storage
DEPENDPATH += $$PWD/../storage

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../storage/release/libstorage.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../storage/debug/libstorage.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../storage/release/storage.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../storage/debug/storage.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../storage/libstorage.a

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../rpclib-master/release/ -lrpc
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../rpclib-master/debug/ -lrpc
else:unix: LIBS += -L$$PWD/../../../rpclib-master/ -lrpc

INCLUDEPATH += $$PWD/../../../rpclib-master
DEPENDPATH += $$PWD/../../../rpclib-master

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../rpclib-master/release/librpc.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../rpclib-master/debug/librpc.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../../rpclib-master/release/rpc.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../../rpclib-master/debug/rpc.lib
else:unix: PRE_TARGETDEPS += $$PWD/../../../rpclib-master/librpc.a

#include <QCoreApplication>
#include <rpc/server.h>
#include "storage.h"
#include "remoteui.h"
#include "sqlitestorage.h"

int main(int argc, char *argv[]) {

    QCoreApplication a(argc, argv);
    Storage * tmp = new SqliteStorage("../../data/sqlite");
    tmp->open();
    RemoteUi server(tmp);
    server.start();
    tmp->close();
    return a.exec();

}

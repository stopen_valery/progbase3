#ifndef REMOTEUI_H
#define REMOTEUI_H
#include "storage.h"
#include <rpc/server.h>
#include <QDebug>

class RemoteUi
{
public:
    RemoteUi(Storage * storage_);
    void start();

private:
    Storage * storage_;
};

#endif // REMOTEUI_H

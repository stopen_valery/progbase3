#include "remoteui.h"
#include <rpc/this_session.h>
#include <rpc/server.h>
#include <iostream>
#include <QDebug>

RemoteUi::RemoteUi(Storage * storage_) : storage_(storage_){}

void RemoteUi::start() {

    rpc::server srv(8080);
    srv.suppress_exceptions(true);

    srv.bind("getAllUsers",[&]() {

        qDebug() << "Get All Users" ;
        return storage_->getAllUsers();

    });

    srv.bind("getUserById",[&](int user_id) {
        qDebug() << "Get User By Id" ;

        return storage_->getUserById(user_id);

    });

    srv.bind("updateUser",[&](const User &user) {
        qDebug() << "Update User" ;

        return storage_->updateUser(user);

    });

    srv.bind("removeUser",[&](int user_id) {
        qDebug() << "Remove User" ;

        return storage_->removeUser(user_id);

    });

    srv.bind("insertUser",[&](const User &user) {
        qDebug() << "Insert User" ;

        return storage_->insertUser(user);

    });

    srv.bind("pagination_user",[&](std::string search, int page_num, int user_id, int f_followers, int t_followers, int  f_following, int t_following, int sort) {

        qDebug() << "Pagination User" ;
        return storage_->pagination_user(search, page_num, user_id, f_followers, t_followers, f_following, t_following, sort);

    });

    srv.bind("get_num",[&](std::string search, int user_id, int f_followers, int t_followers, int  f_following, int t_following) {
        qDebug() << "Get Num" ;

        return storage_->get_num(search, user_id, f_followers, t_followers, f_following, t_following);

    });

    srv.bind("getAllPosts",[&]() {
        qDebug() << "Get All Posts" ;

        return storage_->getAllPosts();

    });

    srv.bind("getPostById",[&](int post_id) {
        qDebug() << "Get Post By Id" ;

        return storage_->getPostById(post_id);

    });

    srv.bind("updatePost",[&](const Post &post) {
        qDebug() << "Update Post" ;

        return storage_->updatePost(post);

    });

    srv.bind("removePost",[&](int post_id) {
        qDebug() << "Remove Post" ;

        return storage_->removePost(post_id);

    });

    srv.bind("insertPost",[&](const Post &post) {
        qDebug() << "Insert Post" ;

        return storage_->insertPost(post);

    });

    srv.bind("insertDBUser",[&](const DB_User &user) {
        qDebug() << "Insert DBUser" ;

        return storage_->insertDBUser(user);

    });

    srv.bind("getUserAuth",[&](std::string & username, std::string & password) {
        qDebug() << "Get User Auth" ;

        return storage_->getUserAuth(username, password);

    });

    srv.bind("find_user_by_name",[&](const string & username) {
        qDebug() << "Find User By Name" ;

        return storage_->find_user_by_name(username);

    });

    srv.bind("getAllUserUsers",[&](int db_user_id) {
        qDebug() << "Get All User Users" ;

        return storage_->getAllUserUsers(db_user_id);

    });

    srv.bind("insertDBUserId",[&](int db_user_id, int user_id) {
        qDebug() << "Insert DBUser Id" ;

        return storage_->insertDBUserId(db_user_id, user_id);

    });

    srv.bind("getAllUserPosts",[&](int user_id) {
        qDebug() << "Get All User Posts" ;

        return storage_->getAllUserPosts(user_id);

    });

    srv.bind("insertUserPost",[&](int user_id, int post_id) {
        qDebug() << "Insert User Post" ;

        return storage_->insertUserPost(user_id, post_id);

    });

    srv.bind("removeUserPost",[&](int user_id, int post_id) {
        qDebug() << "Remove User Post" ;

        return storage_->removeUserPost(user_id, post_id);

    });

    srv.bind("searchPost",[&](int user_id, std::string search, int f_likes, int t_likes, int f_comments, int t_comments) {

        qDebug() << "Search Post" ;
        return storage_->searchPost(user_id, search, f_likes, t_likes, f_comments, t_comments);

    });

    int thread_count = 5;
        srv.async_run(thread_count);
        std:string ending_string;

        while (ending_string != "exit") {

            getline(std::cin, ending_string);
            qDebug() << QString::fromStdString(ending_string);

        }

        exit(0);

}


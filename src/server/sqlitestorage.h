#pragma once

#include <string>
#include <vector>
#include <QtSql>
#include <QSqlDatabase>
#include <QSqlQuery>


#include "storage.h"
#include "user.h"
#include "post.h"

using namespace std;

class SqliteStorage : public Storage
{
 protected:
   QSqlDatabase database_;

 public:
   SqliteStorage(const string & dir_name) : Storage(dir_name) {}

   bool isOpen() const;
   bool open();
   void close();

   // users
   vector<User> getAllUsers(void);
   User getUserById(int user_id);
   bool updateUser(const User &user);
   bool removeUser(int user_id);
   int insertUser(const User &user);
   vector<User> pagination_user(std::string search, int page_num, int user_id, int f_followers, int t_followers, int  f_following, int t_following, int sort);
   int get_num (std::string search, int user_id, int f_followers, int t_followers, int  f_following, int t_following);


   // posts
   vector<Post> getAllPosts(void);
   Post getPostById(int post_id);
   bool updatePost(const Post &post);
   bool removePost(int post_id);
   int insertPost(const Post &post);


   // db_users
   int insertDBUser (const DB_User &user) ;
   DB_User getUserAuth(
       const string & username,
       const string & password);
  DB_User find_user_by_name(
       const string & username);
   vector<User> getAllUserUsers(int db_user_id);
   bool insertDBUserId(int db_user_id, int user_id) ;

   // links
   vector<Post> getAllUserPosts(int user_id);
   bool insertUserPost(int user_id, int post_id);
   bool removeUserPost(int user_id, int post_id);
   vector <Post> searchPost (int user_id, std::string search, int f_likes, int t_likes, int f_comments, int t_comments) ;
};

#include "sqlitestorage.h"
#include <QDebug>

bool SqliteStorage::isOpen() const
{
    return database_.isOpen();
}

bool SqliteStorage::open()
{
    QString path ;
    path = path.fromStdString(name()+"/data.sqlite") ;
    if (!QFileInfo::exists(path))
    {
        qDebug() << "DataBase does not exist:" << path;
        exit(1) ;
    }
    database_ = QSqlDatabase::addDatabase("QSQLITE");
    database_.setDatabaseName(path);
    bool connected = database_.open();
    if (!connected)
    {
        qDebug() << "Opening DataBase error:"
                 << database_.lastError();
    }
}

void SqliteStorage::close()
{
    database_.close() ;
}

// users
vector<User> SqliteStorage::getAllUsers(void)
{
    if(!isOpen())
    {
        open() ;
    }
    vector <User> users ;
    QSqlQuery query("SELECT * FROM users");
    int idNick = query.record().indexOf("nickname");
    int idFollowing = query.record().indexOf("following") ;
    int idBio = query.record().indexOf("bio") ;
    int idFollowers = query.record().indexOf("followers") ;
    int idPhoto = query.record().indexOf("photo") ;
    while (query.next())
    {
        User temp ;
        temp.ID = query.value("ID").toInt();
        QString nick = query.value(idNick).toString();
        temp.nick = nick.toStdString();
        temp.following = query.value(idFollowing).toInt() ;
        temp.followers = query.value(idFollowers).toInt() ;
        QString bio = query.value(idBio).toString() ;
        temp.bio = bio.toStdString() ;
        QByteArray photo = query.value(idPhoto).toByteArray() ;
        temp.photo = photo.toStdString() ;
        users.push_back(temp) ;
    }
    return users ;
}

User SqliteStorage::getUserById(int user_id)
{
    QSqlQuery query;
    if (!query.prepare("SELECT * FROM users WHERE ID = :user_id"))
    {
        qDebug() << "Get User query prepare ERROR:" << query.lastError();
        exit(1) ;
    }
    query.bindValue(":user_id", user_id);
    if (!query.exec())
    {
        qDebug() << "Get User query exec ERROR:" << query.lastError();
        exit(1) ;
    }
    User user ;
    if (query.next())
    {
        int idNick = query.record().indexOf("nickname") ;
        int idFollowers = query.record().indexOf("followers") ;
        int idBio = query.record().indexOf("bio") ;
        int idFollowing = query.record().indexOf("following") ;
        int idPhoto = query.record().indexOf("photo") ;

        user.ID = query.value("ID").toInt();
        QString nick = query.value(idNick).toString();
        user.nick = nick.toStdString();
        user.followers = query.value(idFollowers).toInt() ;
        user.following = query.value(idFollowing).toInt() ;
        QString bio = query.value(idBio).toString() ;
        user.bio = bio.toStdString() ;
        QByteArray photo = query.value(idPhoto).toByteArray() ;
        user.photo = photo.toStdString() ;
        return user ;
    } else {
        user.ID = -1 ;
        return user ;
    }

}

bool SqliteStorage::updateUser(const User &user)
{
    QString ID = ID.number(user.ID) ;
    QString nickname = nickname.fromStdString(user.nick) ;
    QString followers = followers.number(user.followers) ;
    QString following = following.number(user.following) ;
    QString bio = bio.fromStdString(user.bio) ;
    QByteArray photo = photo.fromStdString(user.photo) ;
    QSqlQuery query;
    if (!query.prepare("UPDATE users SET nickname = :nickname, followers=:followers, following=:following, bio=:bio, photo=:photo WHERE ID = :ID")){
        qDebug() << "Update User query prepare ERROR:" << query.lastError();
        exit(1) ;
    }
    query.bindValue(":nickname", nickname);
    query.bindValue(":followers", followers);
    query.bindValue(":following", following);
    query.bindValue(":bio", bio);
    query.bindValue(":photo", photo);
    query.bindValue(":ID", ID);
    if (!query.exec()){
        qDebug() << "Update User query exec ERROR:" << query.lastError();
        exit(1) ;
    }
}

bool SqliteStorage::removeUser(int user_id)
{
    QSqlQuery query;
    int ID = user_id ;
    if (!query.prepare("DELETE FROM users WHERE ID = :ID")){
        qDebug() << "Delete User query prepare ERROR:" << query.lastError();
        exit(1) ;
    }
    query.bindValue(":ID", ID);
    if (!query.exec()){
        qDebug() << "Delete User query exec ERROR"
                    ":" << query.lastError();
        exit(1) ;
    }
}

int SqliteStorage::insertUser(const User &user)
{
    QString nickname = nickname.fromStdString(user.nick) ;
    QString followers = followers.number(user.followers) ;
    QString following = following.number(user.following) ;
    QString bio = bio.fromStdString(user.bio) ;
    QByteArray photo = QByteArray::fromStdString(user.photo) ;
    QSqlQuery query;
    if (!query.prepare("INSERT INTO users (nickname, followers, following, bio, photo) "
                       "VALUES (:nickname, :followers, :following, :bio, :photo)"))
    {
        QSqlError error = query.lastError();
        throw error;
    }
    query.bindValue(":nickname", nickname);
    query.bindValue(":followers", followers);
    query.bindValue(":following", following);
    query.bindValue(":bio", bio);
    query.bindValue(":photo", photo);
    if (!query.exec())
    {
        QSqlError error = query.lastError();
        throw error;
    }
    QVariant id = query.lastInsertId();
    return id.toInt();
}

vector<User> SqliteStorage::pagination_user(std::string search, int page_num, int user_id, int f_followers, int t_followers, int  f_following, int t_following, int sort)
{
    QSqlQuery query;
    QString search_text = QString::fromStdString(search) ;
    int page_size  = 10 ;
    int skippet_items = page_size*(page_num-1) ;
    if(sort == 0)
    {
        if (!query.prepare("SELECT * FROM users WHERE user_id =:user_id "
                           "AND (followers> :f_followers)AND (followers< :t_followers) "
                           "AND (following> :f_following)AND (following< :t_following)"
                           "AND nickname LIKE ('%' || :search_text || '%')  LIMIT:page_size  OFFSET :skippet_items "))
        {
            qDebug() << "ERROR" << query.lastError();
            exit(1) ;
        }
    }
    if(sort == 1)
    {
        if (!query.prepare("SELECT * FROM users WHERE (user_id =:user_id "
                           "AND (followers> :f_followers)AND (followers< :t_followers) "
                           "AND (following> :f_following)AND (following< :t_following)"
                           "AND nickname LIKE ('%' || :search_text || '%')) "
                            "ORDER BY nickname "
                           "LIMIT:page_size  OFFSET :skippet_items"
                           ))
        {
            qDebug() << "ERROR" << query.lastError();
            exit(1) ;
        }
    }
    if(sort == 2)
    {
        if (!query.prepare("SELECT * FROM users WHERE (user_id =:user_id "
                           "AND (followers> :f_followers)AND (followers< :t_followers) "
                           "AND (following> :f_following)AND (following< :t_following)"
                           "AND nickname LIKE ('%' || :search_text || '%')) "
                            "ORDER BY followers DESC "
                           "LIMIT:page_size  OFFSET :skippet_items"
                           ))
        {
            qDebug() << "ERROR" << query.lastError();
            exit(1) ;
        }
    }

    query.bindValue(":user_id", user_id);
    query.bindValue(":page_size", page_size);
    query.bindValue(":skippet_items", skippet_items);
    query.bindValue(":search_text", search_text);
    query.bindValue(":f_followers", f_followers);
    query.bindValue(":t_followers", t_followers);
    query.bindValue(":f_following", f_following);
    query.bindValue(":t_following", t_following);
    if (!query.exec())
    {
        qDebug() << "Get User query exec ERROR:" << query.lastError();
        exit(1) ;
    }
    vector <User> users ;
    int idNick = query.record().indexOf("nickname");
    int idFollowers = query.record().indexOf("followers") ;
    int idBio = query.record().indexOf("bio") ;
    int idFollowing = query.record().indexOf("following") ;
    int idPhoto = query.record().indexOf("photo") ;
    while (query.next())
    {
        User temp ;
        temp.ID = query.value("ID").toInt();
        QString nick = query.value(idNick).toString();
        temp.nick = nick.toStdString();
        temp.following = query.value(idFollowing).toInt() ;
        temp.followers = query.value(idFollowers).toInt() ;
        QString bio = query.value(idBio).toString() ;
        temp.bio = bio.toStdString() ;
        QByteArray photo = query.value(idPhoto).toByteArray() ;
        temp.photo = photo.toStdString() ;
        users.push_back(temp) ;
    }
    return users ;
}

int SqliteStorage::get_num (std::string search, int user_id, int f_followers, int t_followers, int  f_following, int t_following)
{
    QString s = QString::fromStdString(search) ;
    QSqlQuery query;
    query.prepare("SELECT COUNT(*) FROM users WHERE user_id=:user_id "
                  "AND (followers> :f_followers) AND (followers <:t_followers)"
                  "AND (following> :f_following) AND (following <:t_following)"
                  "AND nickname LIKE ('%' || :search_text || '%') ");
    query.bindValue(":user_id", user_id);
    query.bindValue(":search_text", s);
    query.bindValue(":f_followers", f_followers);
    query.bindValue(":t_followers", t_followers);
    query.bindValue(":f_following", f_following);
    query.bindValue(":t_following", t_following);
    if(!query.exec())
    {
        qDebug()<<"Get count error"<<query.lastError();
    }
    if(query.next())
    {
        int items_total = query.value(0).toInt();
        return items_total ;
    }

}


// posts
vector<Post> SqliteStorage::getAllPosts(void)
{
    if(!isOpen())
    {
        open() ;
    }
    vector <Post> posts ;
    QSqlQuery query("SELECT * FROM posts");
    int idPlace = query.record().indexOf("place");
    int idDesc = query.record().indexOf("description") ;
    int idLikes = query.record().indexOf("likes") ;
    int idComs = query.record().indexOf("comments") ;
    int idPhoto = query.record().indexOf("photo") ;
    while (query.next())
    {
        Post temp ;
        temp.ID = query.value("ID").toInt();
        QString place = query.value(idPlace).toString();
        temp.place = place.toStdString();
        temp.likes = query.value(idLikes).toInt() ;
        temp.comments = query.value(idComs).toInt() ;
        QString desc = query.value(idDesc).toString() ;
        temp.description = desc.toStdString() ;
        QByteArray photo = query.value(idPhoto).toByteArray() ;
        temp.photo = photo.toStdString() ;
        posts.push_back(temp) ;
    }
    return posts ;
}

Post SqliteStorage::getPostById(int post_id)
{
    QSqlQuery query;
    if (!query.prepare("SELECT * FROM posts WHERE ID = :post_id"))
    {
        qDebug() << "get post query prepare error:" << query.lastError();
        exit(1) ;
    }
    query.bindValue(":post_id", post_id);
    if (!query.exec())
    {
        qDebug() << "get post query exec error:" << query.lastError();
        exit(1) ;
    }
    Post post ;
    if (query.next())
    {
        int idPlace = query.record().indexOf("place");
        int idDesc = query.record().indexOf("description") ;
        int idLikes = query.record().indexOf("likes") ;
        int idComs = query.record().indexOf("comments") ;
        int idPhoto = query.record().indexOf("photo") ;
        post.ID = query.value("ID").toInt();
        QString place = query.value(idPlace).toString();
        post.place = place.toStdString();
        post.likes = query.value(idLikes).toInt() ;
        post.comments = query.value(idComs).toInt() ;
        QString desc = query.value(idDesc).toString() ;
        post.description = desc.toStdString() ;
        QByteArray photo = query.value(idPhoto).toByteArray() ;
        post.photo = photo.toStdString() ;
        return post ;
    } else
    {
        post.ID = -1 ;
        return post ;
    }
}

bool SqliteStorage::updatePost(const Post &post)
{
    QString ID = ID.number(post.ID) ;
    QString place = place.fromStdString(post.place) ;
    QString likes = likes.number(post.likes) ;
    QString comments = comments.number(post.comments) ;
    QString description = description.fromStdString(post.description) ;
    QByteArray photo = photo.fromStdString(post.photo) ;
    QSqlQuery query;
    if (!query.prepare("UPDATE posts SET place = :place, likes=:likes, comments=:comments, description=:description, photo=:photo WHERE ID = :ID")){
        qDebug() << "updatePerson query prepare error:" << query.lastError();
        exit(1) ;
    }
    query.bindValue(":place", place);
    query.bindValue(":likes", likes);
    query.bindValue(":comments", comments);
    query.bindValue(":description", description);
    query.bindValue(":photo", photo);
    query.bindValue(":ID", ID);
    if (!query.exec()){
        qDebug() << "updatePost query exec error:" << query.lastError();
        exit(1) ;
    }

}

bool SqliteStorage::removePost(int post_id)
{
    QSqlQuery query;
    int ID = post_id ;
    if (!query.prepare("DELETE FROM posts WHERE ID = :ID")){
        qDebug() << "deletePerson query prepare error:" << query.lastError();
        exit(1) ;
    }
    query.bindValue(":ID", ID);
    if (!query.exec()){
        qDebug() << "deletePerson query exec error:" << query.lastError();
        exit(1) ;
    }
}

int SqliteStorage::insertPost(const Post &post)
{
    QString place = place.fromStdString(post.place) ;
    QString likes = likes.number(post.likes) ;
    QString comments = comments.number(post.comments) ;
    QString description = description.fromStdString(post.description) ;
    QByteArray photo = photo.fromStdString(post.photo) ;
    QSqlQuery query;
    if (!query.prepare("INSERT INTO posts (place, likes, comments, description, photo) "
                       "VALUES (:place, :likes, :comments, :description, :photo)"))
    {
        QSqlError error = query.lastError();
        throw error;
    }
    query.bindValue(":place", place);
    query.bindValue(":likes", likes);
    query.bindValue(":comments", comments);
    query.bindValue(":description", description);
    query.bindValue(":photo", photo);
    if (!query.exec())
    {
        QSqlError error = query.lastError();
        throw error;
    }
    QVariant id = query.lastInsertId();
    return id.toInt();
}


//DB_User

int SqliteStorage::insertDBUser (const DB_User &user)
{
    QString username = username.fromStdString(user.username) ;
    QString password = password.fromStdString(user.password_hash) ;
    QSqlQuery query;
    if (!query.prepare("INSERT INTO db_users (username, password_hash) "
                       "VALUES (:username, :password)"))
    {
        QSqlError error = query.lastError();
        throw error;
    }
    query.bindValue(":username", username);
    query.bindValue(":password", password);
    if (!query.exec())
    {
        QSqlError error = query.lastError();
        throw error;
    }
    QVariant id = query.lastInsertId();
    return id.toInt();
}

DB_User SqliteStorage::find_user_by_name(
        const string & username)
{
    QSqlQuery query;
    QString username_s = username_s.fromStdString(username) ;
    if (!query.prepare("SELECT * FROM db_users WHERE username = :username_s"))
    {
        qDebug() << "Get User query prepare ERROR:" << query.lastError();
        exit(1) ;
    }
    query.bindValue(":username_s", username_s);
    if (!query.exec())
    {
        qDebug() << "Get User query exec ERROR:" << query.lastError();
        exit(1) ;
    }
    DB_User user ;
    if (query.next())
    {
        user.ID = query.value("ID").toInt();
        user.username = " " ;
        user.password_hash = " " ;
        return user ;
    }
    else
    {
        user.ID = -1 ;
        user.username = " " ;
        user.password_hash = " " ;
        return user;
    }
}

DB_User SqliteStorage::getUserAuth(
        const string & username,
        const string & password)
{
    QSqlQuery query;
    QString username_s = username_s.fromStdString(username) ;
    QString password_s = password_s.fromStdString(password) ;
    if (!query.prepare("SELECT * FROM db_users WHERE username = :username_s AND password_hash= :password_s"))
    {
        qDebug() << "Get User query prepare ERROR:" << query.lastError();
        exit(1) ;
    }
    query.bindValue(":username_s", username_s);
    query.bindValue(":password_s", password_s);
    if (!query.exec())
    {
        qDebug() << "Get User query exec ERROR:" << query.lastError();
        exit(1) ;
    }
    DB_User user ;
    if (query.next())
    {
        user.ID = query.value("ID").toInt();
        user.password_hash = password ;
        user.username = username ;
        return user ;
    } else {
        user.ID = -1 ;
        return user;
    }

}

vector<User> SqliteStorage::getAllUserUsers(int db_user_id)
{
    QSqlQuery query;
    const char * sql = "SELECT * FROM users "
                       "WHERE user_id = :db_user_id;";
    if (!query.prepare(sql))
    {
        QSqlError error = query.lastError();
        throw error;
    }
    query.bindValue(":db_user_id", db_user_id);
    if (!query.exec())
    {
        QSqlError error = query.lastError();
        throw error;
    }
    vector<User> users;
    while (query.next())
    {
        User temp;
        temp.ID = query.value("ID").toInt();
        QString nickname = query.value("nickname").toString();
        temp.nick = nickname.toStdString() ;
        QString bio = query.value("bio").toString();
        temp.bio = bio.toStdString() ;
        temp.followers = query.value("followers").toInt();
        temp.following = query.value("following").toInt();
        QByteArray photo = query.value("photo").toByteArray();
        temp.photo = photo.toStdString() ;
        users.push_back(temp);
    }
    return users;

}

bool SqliteStorage::insertDBUserId(int db_user_id, int user_id) {

    QSqlQuery query;
    query.prepare("UPDATE users SET user_id = :db_user_id WHERE ID = :user_id");
    query.bindValue(":db_user_id", db_user_id);
    query.bindValue(":user_id", user_id);

    if (!query.exec()) {

        qDebug() << "add user error:" << query.lastError();

        return false;

    }

    if (!query.numRowsAffected()) {

        return false;

    }

    return true;

}


// links
vector<Post> SqliteStorage::getAllUserPosts(int user_id)
{
    QSqlQuery query;
    if (!query.prepare("SELECT * FROM links WHERE user_id = :user_id"))
    {
        qDebug() << "get post query prepare error:" << query.lastError();
        exit(1) ;
    }
    query.bindValue(":user_id", user_id);
    if (!query.exec())
    {
        qDebug() << "get post query exec error:" << query.lastError();
        exit(1) ;
    }
    vector <Post> posts ;
    while (query.next())
    {
        int ID = query.value("post_id").toInt();
        Post post =  this->getPostById(ID) ;
        posts.push_back(post) ;
    }
    return posts ;
}

bool SqliteStorage::insertUserPost(int user_id, int post_id)
{
    QSqlQuery query;
    if (!query.prepare("INSERT INTO links (user_id, post_id) "
                       "VALUES (:user_id, :post_id)"))
    {
        QSqlError error = query.lastError();
        throw error;
    }
    query.bindValue(":user_id", user_id);
    query.bindValue(":post_id", post_id);
    if (!query.exec())
    {
        QSqlError error = query.lastError();
        throw error;
    }
    return true;
}

bool SqliteStorage::removeUserPost(int user_id, int post_id)
{
    QSqlQuery query;
    int ID = post_id ;
    if (!query.prepare("DELETE FROM links WHERE user_id = :user_id AND post_id=:post_id")){
        qDebug() << "delete query prepare error:" << query.lastError();
        exit(1) ;
    }
    query.bindValue(":user_id", user_id);
    query.bindValue(":post_id", post_id) ;
    if (!query.exec()){
        qDebug() << "delete query exec error:" << query.lastError();
        exit(1) ;
    }
    return true ;
}

vector <Post> SqliteStorage::searchPost (int user_id, std::string search, int f_likes, int t_likes, int f_comments, int t_comments)
{
    QSqlQuery query;
    if (!query.prepare("SELECT * FROM links WHERE user_id = :user_id"))
    {
        qDebug() << "get post query prepare error:" << query.lastError();
        exit(1) ;
    }
    query.bindValue(":user_id", user_id);

    if (!query.exec())
    {
        qDebug() << "get post query exec error:" << query.lastError();
        exit(1) ;
    }
    vector <Post> posts ;
    while (query.next())
    {
        int ID = query.value("post_id").toInt();
        QSqlQuery quer;
        if (!quer.prepare("SELECT * FROM posts WHERE ID = :post_id AND place LIKE ('%' || :search_text || '%')"
                          "AND (likes>:f_likes) AND (likes<:t_likes)"
                          "AND (comments>:f_comments) AND (comments<:t_comments)"))
        {
            qDebug() << "get post query prepare error:" << query.lastError();
            exit(1) ;
        }
        quer.bindValue(":post_id", ID);
        quer.bindValue(":f_likes", f_likes);
        quer.bindValue(":t_likes", t_likes);
        quer.bindValue(":f_comments", f_comments);
        quer.bindValue(":t_comments", t_comments);
        quer.bindValue(":search_text", QString::fromStdString(search));
        if (!quer.exec())
        {
            qDebug() << "get post query exec error:" << query.lastError();
            exit(1) ;
        }
        Post post ;
        if (quer.next())
        {
            int idPlace = quer.record().indexOf("place");
            int idDesc = quer.record().indexOf("description") ;
            int idLikes = quer.record().indexOf("likes") ;
            int idComs = quer.record().indexOf("comments") ;
            int idPhoto = quer.record().indexOf("photo") ;
            post.ID = quer.value("ID").toInt();
            QString place = quer.value(idPlace).toString();
            post.place = place.toStdString();
            post.likes = quer.value(idLikes).toInt() ;
            post.comments = quer.value(idComs).toInt() ;
            QString desc = quer.value(idDesc).toString() ;
            post.description = desc.toStdString() ;
            QByteArray photo = quer.value(idPhoto).toByteArray() ;
            post.photo = photo.toStdString() ;
            posts.push_back(post);
        }


    }

    return posts ;
}

#pragma once

#include <string>
#include <vector>

#include <experimental/optional>
#include "user.h"
#include "post.h"
#include "db_user.h"

using namespace std;

class Storage
{
 private:
   string dir_name_;

 public:
   Storage();
   explicit Storage(const string & dir_name);
   virtual ~Storage() {}

   void setName(const string & dir_name);
   string name() const;

   virtual bool isOpen() const = 0;
   virtual bool open() = 0;
   virtual void close() = 0;

   // users
   virtual vector<User> getAllUsers(void) = 0;
   virtual User getUserById(int user_id) = 0;
   virtual bool updateUser(const User &user) = 0;
   virtual bool removeUser(int student_id) = 0;
   virtual int insertUser(const User &user) = 0;
   virtual vector<User> pagination_user(std::string search, int page_num, int user_id, int f_followers, int t_followers, int  f_following, int t_following, int sort)= 0;
   virtual int get_num (std::string search, int user_id, int f_followers, int t_followers, int  f_following, int t_following) = 0;

   // posts
   virtual vector<Post> getAllPosts(void) = 0;
   virtual Post getPostById(int course_id) = 0;
   virtual bool updatePost(const Post &course) = 0;
   virtual bool removePost(int post_id) = 0;
   virtual int insertPost(const Post &post) = 0;



   // db_users
   virtual int insertDBUser (const DB_User &user) = 0;
   virtual DB_User getUserAuth(
       const string & username,
       const string & password) = 0;
   virtual DB_User find_user_by_name(
       const string & username) = 0;
   virtual vector<User> getAllUserUsers(int db_user_id) = 0;
   virtual bool insertDBUserId(int db_user_id, int user_id) = 0 ;

   // links
   virtual vector<Post> getAllUserPosts(int user_id) = 0;
   virtual bool insertUserPost(int user_id, int post_id) = 0;
   virtual bool removeUserPost(int user_id, int post_id) = 0;
   virtual vector <Post> searchPost (int user_id, std::string search, int f_likes, int t_likes, int f_comments, int t_comments) = 0;
};


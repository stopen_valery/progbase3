#ifndef DB_USER
#define DB_USER

#include <QVariant>

#include <rpc/msgpack.hpp>

using namespace std ;

struct DB_User
{
    int ID ;
    string username ;
    string password_hash ;
    MSGPACK_DEFINE(ID, username, password_hash)
};


#endif // DB_USER


Q_DECLARE_METATYPE (DB_User)

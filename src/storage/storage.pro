#-------------------------------------------------
#
# Project created by QtCreator 2020-05-20T12:16:24
#
#-------------------------------------------------

QT       -= gui

QT += sql
QT += xml
QT += core

TARGET = storage
TEMPLATE = lib
CONFIG += staticlib

SOURCES += storage.cpp

QMAKE_CXXFLAGS += -std=c++17


HEADERS += storage.h \
    user.h \
    post.h \
    db_user.h
unix {
    target.path = /usr/lib
    INSTALLS += target
}

#include <QVariant>

#pragma once
#include <cstdio>
#include <string>

#include <rpc/msgpack.hpp>

struct User
{
    int ID ;
    std::string nick ;
    int followers ;
    int following ;
    std::string bio ;
    std::string photo ;
    MSGPACK_DEFINE(ID, nick, followers, following, bio, photo)
} ;

Q_DECLARE_METATYPE(User)

#pragma once
#include <cstdio>
#include <string>
#include <QVariant>

#include <rpc/msgpack.hpp>

struct Post
{
    int ID ;
    std::string description ;
    std::string place ;
    int likes ;
    int comments ;
    std::string photo ;
    MSGPACK_DEFINE(ID, description, place, likes, comments, photo)
} ;

Q_DECLARE_METATYPE(Post)

